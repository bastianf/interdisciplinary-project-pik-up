#!/bin/sh

runs='00'
init='01'

date=$(date -d '-1 day' '+%Y%m%d')

wget -N https://nomads.ncep.noaa.gov/pub/data/nccf/com/cfs/prod/cfs.${date}/${runs}/time_grib_${init}/z500.${init}.${date}${runs}.daily.grb2 -O latest.grb2

cdo -f nc seltimestep,1/120 -sellonlatbox,-40,50,30,70 -daymean -remapbil,targetgrid latest.grb2 'CFS_Z500_EU_'${date}'_'${runs}'_'${init}'_120d.nc'

rm *.grb2