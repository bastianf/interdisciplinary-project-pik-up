#!/usr/bin/env python
# -*- coding: utf-8 -*-

HEADER = '''
**Header:**
<br/>
* title               :gfswt.py
* description         :detection of weather-types in forecasts.
* author              :Peter Hoffmann
* email               :peterh@pik-potsdam.de
* affiliation         :potsdam institute for climate impacts research (PIK)
* research department :climate resilience
* working group       :hydro-climatic risks
* project             :captainrain
* date                :2024-01-25
* version             :1.0
* usage               :python3 gfswt.py
* python_version      :3.10.0  
***
**Requirements**
<br/>
* python-dateutil==2.8.2
* siphon==0.9
* matplotlib==3.8.2
* numpy==1.23.1
* netCDF4==1.6.5
* Cartopy==0.22.0
* scipy==1.8.0
* xarray==2023.10.1
* rich==13.7.0
***
'''

import os
import sys
from datetime import datetime, timedelta,date
from siphon.catalog import TDSCatalog
from siphon.ncss import NCSS
import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
import matplotlib.pyplot as P
import numpy as N
from netCDF4 import Dataset, num2date,date2num
from matplotlib.offsetbox import AnchoredText
import cartopy.crs as ccrs
from scipy.interpolate import griddata
import matplotlib.image as image
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier

from xarray.backends import NetCDF4DataStore
import xarray as xr
import warnings
warnings.filterwarnings('ignore')
from rich.console import Console
from rich.markdown import Markdown
from rich.table import Table

console = Console(record=True)

P.style.use('classic')
P.style.use('bmh')   
   
params = {'legend.fontsize': 8,
          'grid.color':'black', 
          'axes.grid':True,
          'grid.linestyle':'dotted',
          'xtick.direction':'out',
          'ytick.direction':'out',
          'xtick.labelsize':14,
          'ytick.labelsize':14,
          'font.family': 'serif'}

P.rcParams.update(params)

EU_GWL = {'W':'windy','BM':'bright','TRM':'rainy','TRW':'thunderstorm','HM':'sunny','SW':'hot','NW':'icy','HB':'icy','HF':'sunny','WS':'bright','TB':'stormy','HN':'sunny','WW':'windy','TM':'flood','SE':'hot','N':'icy','HNF':'sunny','S':'hot','NE':'icy'}
JO_OBJ = {'WT01':'bright','WT02':'bright','WT03':'sunny','WT04':'bright','WT05':'sunny','WT06':'rainy','WT07':'sunny','WT08':'rainy','WT09':'bright','WT10':'bright','WT11':'bright','WT12':'sunny','WT13':'rainy','WT14':'bright','WT15':'stormy','WT16':'bright','WT17':'flood','WT18':'flood','WT19':'bright','WT20':'sunny'}
EU_OBJ = {'WT01':'windy','WT02':'windy','WT03':'bright','WT04':'hot','WT05':'icy','WT06':'icy','WT07':'sunny','WT08':'rainy','WT09':'stormy','WT10':'thunderstorm','WT11':'rainy','WT12':'icy','WT13':'icy','WT14':'stormy','WT15':'sunny','WT16':'bright','WT17':'rainy','WT18':'flood','WT19':'thunderstorm','WT20':'rainy'}

gs = EU_GWL

def mf(List):
    return max(set(List), key = List.count)

#bundle_dir = getattr(sys, '_MEIPASS', os.path.abspath(os.path.dirname(__file__)))
bundle_dir = './data/'

text = '''
# GFSWT-1.0
'''

md = Markdown(text)
console.print(md)

md = Markdown(HEADER)
console.print(md)

text = '''
**Setting:**
<br/>
* European Weather-Types
***
'''

md = Markdown(text)
console.print(md)

text = '''
**Reading:**
<br/>
* ERA5 Reanalysis Data of daily Z500 over Europe
* Time series of Weather-Types
***
'''

md = Markdown(text)
console.print(md)

nc = Dataset('%s/era5_z500_daily_1951-1980_2x2_EU.netcdf'%bundle_dir,'r')
#nc = Dataset('%s/era5_z500_daily_1951-1980_2x2_EU.netcdf'%bundle_dir,'r')

lons = N.array(nc.variables['lon'][:]);nx = len(lons)
lats = N.array(nc.variables['lat'][:]);ny = len(lats)
dat = N.array(nc.variables['z500'][:])

tim = nc.variables['time']

tim = num2date(tim[:], units=tim.units,calendar=tim.calendar) 

tt = []

for it in tim:

    tt.append('%04i-%02i-%02i'%(it.year,it.month,it.day))
    
tt = N.array(tt)

nc.close()

nd = dat.shape[0]

fileC = '%s/GWL_1951-1980.dat'%bundle_dir
#fileC = '%s/era5_z500_daily_1951-1980_1x1_EU_20.lst'%bundle_dir

csv = N.genfromtxt(fileC,names=True,delimiter=';',dtype=None)

gw = N.array(csv['gw'],str)

gg = gw

gw[gw=='NWZ'] = 'NW'
gw[gw=='NWA'] = 'NW'
gw[gw=='SWZ'] = 'SW'
gw[gw=='SWA'] = 'SW'
gw[gw=='NEZ'] = 'NE'
gw[gw=='NEA'] = 'NE'
gw[gw=='SEZ'] = 'SE'
gw[gw=='SEA'] = 'SE'

gw[gw=='NFZ'] = 'NF'
gw[gw=='NFA'] = 'NF'
gw[gw=='HNFZ'] = 'HNF'
gw[gw=='HNFA'] = 'HNF'
gw[gw=='HFZ'] = 'HF'
gw[gw=='HFA'] = 'HF'
gw[gw=='HNZ'] = 'HN'
gw[gw=='HNA'] = 'HN'

gw[gw=='SZ'] = 'S'
gw[gw=='SA'] = 'S'
gw[gw=='NZ'] = 'N'
gw[gw=='NA'] = 'N'
gw[gw=='WZ'] = 'W'
gw[gw=='WA'] = 'W'
gw[gw=='WS'] = 'W'
gw[gw=='U'] = 'W'
   
gw[gw=='TM'] = 'TRM'   
   
gg = gw

phi = N.zeros((nd,ny,nx),float)

for d in range(nd):

    tmp = dat[d,:,:]
    phi[d,:,:] = (tmp-N.min(tmp))/(N.max(tmp)-N.min(tmp))

phi = N.reshape(phi,(nd,ny*nx))

X_train = phi[:,:]
y_train = gg[:]

text = '''
**Training:**
<br/>
* Decision Tree between Z500 and Weather-Types
***
'''

md = Markdown(text)
console.print(md)

#clf = DecisionTreeClassifier(criterion="gini",random_state=0)
#clf = DecisionTreeClassifier(criterion="entropy",random_state=0)
clf = RandomForestClassifier(n_estimators=100)
clf = clf.fit(X_train,y_train)

os.system('cp %s/ERA5_z500_daily_1981-2022_2x2_EU.dat gcmwt.csv'%(bundle_dir))

f = open('gcmwt.csv','a')
#f.write('ja;mo;ta;gw;gcm\n')

gcms = {'MPI-ESM1-2-HR':'dodgerblue','MPI-ESM1-2-LR':'orangered','EC-Earth3':'lime','MIROC6':'gold','IPSL-CM6A-LR':'m','CNRM-ESM2-1':'y','GFDL-CM4':'gray','CanESM5':'g','NorESM2-MM':'k'}

for gcm in gcms:

    nc = Dataset('%s/%s_z500_daily_1981-2090_2x2_EU.netcdf'%(bundle_dir,gcm),'r')

    lon = N.array(nc.variables['lon'][:]);nx = len(lon)
    lat = N.array(nc.variables['lat'][:]);ny = len(lat)
    dat = N.array(nc.variables['zg500'][:])

    tim = nc.variables['time']

    tim = num2date(tim[:], units=tim.units,calendar=tim.calendar) 

    jj = []
    mm = []
    dd = []
    tt = []

    for it in tim:

        jj.append(int(it.year))
        mm.append(int(it.month))
        dd.append(int(it.day))
        tt.append('%04i-%02i-%02i'%(it.year,it.month,it.day))    
    
    jj = N.array(jj)
    mm = N.array(mm)
    dd = N.array(dd)
    tt = N.array(tt)

    nc.close()

    nd = dat.shape[0]

    dum = N.zeros((nd,ny,nx),float)

    for d in range(nd):

       tmp = dat[d,:,:]
        
       tmp = (tmp-N.min(tmp))/(N.max(tmp)-N.min(tmp))
   
       dum[d,:,:] = tmp
        
    X_test = N.reshape(dum,(nd,ny*nx))

    y_pred = clf.predict(X_test)

    table = Table(title='%s_z500_daily_1981-2090_2x2_EU'%gcm)
    
    table.add_column("Date", justify="left", style="cyan", no_wrap=True)
    table.add_column("GWL", justify="lefz", style="green")

    for d in range(nd):

        f.write('%i;%i;%i;%s;%s\n'%(jj[d],mm[d],dd[d],y_pred[d],gcm))

        table.add_row("%s"%tt[d],"%s"%y_pred[d])
        
    console = Console()
    console.print(table)

f.close()

file = 'gcmwt.csv'

csv = N.genfromtxt(file,names=True,delimiter=';',dtype=None)

gw = N.array(csv['gw'],str)
jj = N.array(csv['ja'],int)
mm = N.array(csv['mo'],int)
dd = N.array(csv['ta'],int)
gc = N.array(csv['gcm'],str)

'''

table = Table(title='Aug2002')

an = N.where((jj==2002)&(mm==8)&(dd==5)&(gc=='ERA5'))[0]
an = an[0]
nt = 10
    
for d in range(an,an+nt,1):
    
    table.add_column("%04i-%02i-%02i"%(jj[d],mm[d],dd[d]), justify="left", style="cyan", no_wrap=True)

table.add_row("%s"%gw[d],"%s"%gw[an],"%s"%gw[an+1],"%s"%gw[an+2],"%s"%gw[an+3],"%s"%gw[an+4],"%s"%gw[an+5],"%s"%gw[an+6],"%s"%gw[an+7],"%s"%gw[an+8])

console = Console()
console.print(table)

table = Table(title='May2013')

an = N.where((jj==2013)&(mm==5)&(dd==25)&(gc=='ERA5'))[0]
an = an[0]
nt = 10
    
for d in range(an,an+nt,1):
    
    table.add_column("%04i-%02i-%02i"%(jj[d],mm[d],dd[d]), justify="left", style="cyan", no_wrap=True)

table.add_row("%s"%gw[d],"%s"%gw[an],"%s"%gw[an+1],"%s"%gw[an+2],"%s"%gw[an+3],"%s"%gw[an+4],"%s"%gw[an+5],"%s"%gw[an+6],"%s"%gw[an+7],"%s"%gw[an+8])

console = Console()
console.print(table)

table = Table(title='Jan2019')

an = N.where((jj==2019)&(mm==1)&(dd==5)&(gc=='ERA5'))[0]
an = an[0]
nt = 10
    
for d in range(an,an+nt,1):
    
    table.add_column("%04i-%02i-%02i"%(jj[d],mm[d],dd[d]), justify="left", style="cyan", no_wrap=True)

table.add_row("%s"%gw[d],"%s"%gw[an],"%s"%gw[an+1],"%s"%gw[an+2],"%s"%gw[an+3],"%s"%gw[an+4],"%s"%gw[an+5],"%s"%gw[an+6],"%s"%gw[an+7],"%s"%gw[an+8])

console = Console()
console.print(table)

table = Table(title='Jul2021')

an = N.where((jj==2021)&(mm==7)&(dd==8)&(gc=='ERA5'))[0]
an = an[0]
nt = 10
    
for d in range(an,an+nt,1):
    
    table.add_column("%04i-%02i-%02i"%(jj[d],mm[d],dd[d]), justify="left", style="cyan", no_wrap=True)

table.add_row("%s"%gw[d],"%s"%gw[an],"%s"%gw[an+1],"%s"%gw[an+2],"%s"%gw[an+3],"%s"%gw[an+4],"%s"%gw[an+5],"%s"%gw[an+6],"%s"%gw[an+7],"%s"%gw[an+8])

console = Console()
console.print(table)

table = Table(title='Jul2022')

an = N.where((jj==2022)&(mm==7)&(dd==15)&(gc=='ERA5'))[0]
an = an[0]
nt = 10
    
for d in range(an,an+nt,1):
    
    table.add_column("%04i-%02i-%02i"%(jj[d],mm[d],dd[d]), justify="left", style="cyan", no_wrap=True)

table.add_row("%s"%gw[d],"%s"%gw[an],"%s"%gw[an+1],"%s"%gw[an+2],"%s"%gw[an+3],"%s"%gw[an+4],"%s"%gw[an+5],"%s"%gw[an+6],"%s"%gw[an+7],"%s"%gw[an+8])

console = Console()
console.print(table)

'''

#go = N.array(['W','BM','TRM','TRW','HM','SW','NW','HB','HF','WS','TB','HN','WW','TM','SE','N','HNF','S','NE'])
go = N.array(['W','BM','TRM','TRW','HM','SW','NW','HB','HF','TB','HN','SE','N','HNF','S','NE'])
#go = N.array(['WT01','WT02','WT03','WT04','WT05','WT06','WT07','WT08','WT09','WT10','WT11','WT12','WT13','WT14','WT15','WT16','WT17','WT18','WT19','WT20'])
ng = len(go)

jo = N.arange(1981,2021,1)
nj = len(jo)

fig = P.figure(figsize=(8,6))
   
P.figtext(.0,1.00,'European Weather-Type Frequency in Climate Scenarios',fontsize=16,weight='bold',ha='left')
P.figtext(.0,0.97,'\xa9 P. Hoffmann (PIK)', fontsize=8, ha='left')

ax = P.subplot(111)
   
P.plot([0,50],[0,50],'k',lw=0.5,label='ERA5')

nn = N.zeros((nj,ng),float)
wp = N.zeros((nj,ng),float)

for j in range(nj):
    for g in range(ng):
    
        id = N.where((jj==jo[j])&(gw==go[g])&(gc=='ERA5'))[0]
        #id = N.where((jj==jo[j])&(gw==go[g])&(gc=='ERA5')&(mm>=6)&(mm<=8))[0]

        if(len(id)>0):

              nn[j,g] = len(id)
            
        ii = []
        
        num = 0       
        
        nt = len(id)
        
        for i in id[0:nt-1]:
        
            if(gw[i]==gw[i+1]):
            
                num = num+1
                
            else:
            
                ii.append(num)
                
                num = 0
                
        ii = N.array(ii)
                
        if(len(ii)>0):       
                
            wp[j,g] = 10.*N.max(ii)

nne = nn
wpe = wp

jo = N.arange(1981,2091,1)
nj = len(jo)

for gcm in gcms:

    nn = N.zeros((nj,ng),float)
    wp = N.zeros((nj,ng),float)

    for j in range(nj):
        for g in range(ng):
    
            id = N.where((jj==jo[j])&(gw==go[g])&(gc==gcm))[0]
            #id = N.where((jj==jo[j])&(gw==go[g])&(gc==gcm)&(mm>=6)&(mm<=8))[0]

            if(len(id)>0):

               nn[j,g] = len(id)
            
            ii = []
        
            num = 0       
        
            nt = len(id)
        
            for i in id[0:nt-1]:
        
                if(gw[i]==gw[i+1]):
            
                    num = num+1
                
                else:
            
                    ii.append(num)
                
                    num = 0
                
            ii = N.array(ii)
                
            if(len(ii)>0):       
                
                wp[j,g] = 10.*N.max(ii)
        
    err = []    
        
    for g in range(ng):

        x = 100.*N.sum(nn[0:42,g])/(42.*365.)
        y = 100.*N.sum(nn[nj-42::,g])/(42.*365.)
        z = 100.*N.sum(nne[0:42,g])/(42.*365.)

        if((go[g]=='W')|(go[g]=='TRM')|(go[g]=='HB')):

        #P.scatter(z,x,s=N.mean(wp[0:40,g]),marker='o',ec='k',fc=gcms[gcm],zorder=5)
        #P.scatter(z,y,s=N.mean(wp[nj-40:,g]),marker='s',ec='k',fc=gcms[gcm],zorder=5)
           P.text(z,x,'%s'%go[g],color=gcms[gcm],fontsize=8,ha='center',va='center',weight='bold',zorder=6)
           P.text(z,y,'%s'%go[g],color=gcms[gcm],fontsize=8,ha='center',va='center',weight='bold',zorder=6) 
        
        else:

           P.text(z,x,'%s'%go[g],color=gcms[gcm],fontsize=8,ha='center',va='center',weight='bold',zorder=6,alpha=0.3)
           P.text(z,y,'%s'%go[g],color=gcms[gcm],fontsize=8,ha='center',va='center',weight='bold',zorder=6,alpha=0.3)
        
        err.append(N.abs(x-z))
        
    err = N.array(err)    
    err = N.mean(err)    
        
    P.scatter(0,0,s=15,ec='k',fc=gcms[gcm],zorder=5,label='%s: %.1f'%(gcm,err))

at = AnchoredText('1981-2022',prop=dict(size=10,weight='bold'),frameon=True,loc='upper left')
at.patch.set_boxstyle("round,pad=0.,rounding_size=0.1")
ax.add_artist(at)

P.semilogx()
P.semilogy()

P.xticks([2,3,4,5,10,20,30],['2%','3%','4%','5%','10%','20%','30%'])
P.yticks([2,3,4,5,10,20,30],['2%','3%','4%','5%','10%','20%','30%'])

P.xlim(0.5,50)
P.ylim(0.5,50)
    
P.xlabel('Weather-Type Frequency | ERA5',fontsize=14,weight='bold')
P.ylabel('Weather-Type Frequency | CMIP6',fontsize=14,weight='bold')

P.text(4,1.8,'underestimation',color='w',fontsize=20,ha='left',va='center')
P.text(4,22,'overestimation',color='w',fontsize=20,ha='left',va='center')
    
P.legend(loc=4,shadow=True)

P.tight_layout()

P.savefig('gcmwt.png',dpi=240,bbox_inches= 'tight')
