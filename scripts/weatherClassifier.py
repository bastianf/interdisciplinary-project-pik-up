#!/bin/python

import numpy as np
#import pandas as pd
#import xarray as xr
import csv
import os
import joblib
from netCDF4 import Dataset
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


class weatherClassifier():
    training_data_x = None
    training_data_y = None
    classifier = None
    
    def __init__(self, id):
        print("Weather Classifier with id {} created.".format(id))
    
    # Inputs are file paths to the netCDF files for the x and y training data
    def read_training_data(self, training_data_x, training_data_y):
        self.training_data_x = Dataset(training_data_x, 'r')
        
        csv = np.genfromtxt(training_data_y, names=True,delimiter=';',dtype=None)

        self.training_data_y = np.array(csv['gw'],str)
        
        #self.training_data_y = Dataset(training_data_y, 'r')
    
    def train(self):
        self.classifier = RandomForestClassifier(n_estimators=100)
        X_data = np.array(self.training_data_x.variables['z500'])
        num_time = X_data.shape[0]
        
        # Rescale x
        phi = np.zeros((num_time, X_data.shape[1], X_data.shape[2]), float)

        for d in range(num_time):
            tmp = X_data[d,:,:]
            phi[d,:,:] = (tmp-np.min(tmp))/(np.max(tmp)-np.min(tmp))
            
        X_data = np.reshape(phi, (num_time, X_data.shape[1] * X_data.shape[2]))
        
        self.classifier = self.classifier.fit(X_data, self.training_data_y)
        print("Successfully trained!")
        
    def predict(self, X_data, output_dir, output_name):
        # Load X_data and its dimenions lenghts
        X_data = Dataset(X_data, 'r')
        X_data = np.array(X_data.variables['zg500'])
        num_time = X_data.shape[0]
        num_lat = X_data.shape[2]
        num_lon = X_data.shape[3]
         
        # Rescale x to:  x_ij - min(x) / max(x) - min(x)
        phi = np.zeros((num_time, num_lat, num_lon),float)
        
        for d in range(num_time):
           tmp = X_data[d,:,:]
           phi[d,:,:] = (tmp-np.min(tmp))/(np.max(tmp)-np.min(tmp))
        
        # Reshape to 2D and predict
        X_data = np.reshape(phi, (num_time, num_lat * num_lon))
        y_pred = self.classifier.predict(X_data)
        
        # Save results as csv
        with open(os.path.join(output_dir, output_name), mode='w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(y_pred)
        print(y_pred)    
    
    def evaluate(self, test_size=0.01, iterations=10):
        print("Test size = {}".format(test_size))
        print("Number of testing iterations = {}".format(iterations))
        
        # TODO: Check if training data is available
        
        score_list = []
        for i in range(iterations):
            # Split into test and training data
            X_train, X_test, y_train, y_test = train_test_split(self.training_data_x, self.training_data_y, test_size=test_size)
            
            # Train and predict
            ev_model = RandomForestClassifier(n_estimators=100)
            ev_model.fit(X_train, y_train)
            y_predict = ev_model.predict(x_test)
            
            # Add accuracy score to list
            score_list.append(accuracy_score(y_test, y_predict))
        
        # Print and return mean accuracy over all iterations
        mean_accuracy = sum(score_list) / len(score_list)    
        print("The evaluated accuracy is: {}". format(mean_accuracy))
        return mean_accuracy
    
    def save_model(self, path, name):
        joblib.dump(self.classifier, os.path.join(path,name))
        print("Model saved under: {}/{}".format(path, name))
    
    def load_model(self, path, name):
        self.classifier = joblib.load(os.path.join(path, name))
        print("Model loaded from: {}/{}".format(path, name))
        
# Test area
wc1 = weatherClassifier(1)

#x = np.ndarray([0,1,2,3,4,5], [0,1,2,3,4,5])
#y = np.ndarray([0,1,2,3,4,5], [0,1,0,1,0,1])
training_data_dir = "/home/bastian/Documents/university/interdisciplinary-project-pik-up/data/training"

wc1.read_training_data(os.path.join(training_data_dir, "era5_z500_daily_1951-1980_2x2_EU.netcdf"), os.path.join(training_data_dir, "GWL_1951-1980.dat"))
#print(wc1.training_data_y)
#wc1.train()
wc1.load_model(training_data_dir, "test-model1")
wc1.predict("/home/bastian/Documents/university/interdisciplinary-project-pik-up/data/to_classify/MPI-ESM1-2-HR_z500_daily_1981-2090_2x2_EU.netcdf", "/home/bastian/Documents/university/interdisciplinary-project-pik-up/data/results", "test_predict1")

#wc1.save_model(training_data_dir, "test-model1")
#print(wc1.training_data_y)

        
